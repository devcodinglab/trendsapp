define(["exports","require","./my-appnav.js"],function(_exports,_require,_myAppnav){"use strict";Object.defineProperty(_exports,"__esModule",{value:!0});_exports.MyApp=_exports.AppLayoutBehavior=_exports.$myApp=_exports.$appLayoutBehavior=void 0;_require=babelHelpers.interopRequireWildcard(_require);function _templateObject5_ba974b809c2211eaa17be74ebc068880(){var data=babelHelpers.taggedTemplateLiteral(["\n      <style>\n        :host {\n          --app-primary-color: #062146;\n          --app-secondary-color: black;\n          --app-tertiary-color: #004481;\n\n          display: block;\n        }\n\n        app-drawer-layout:not([narrow]) [drawer-toggle] {\n          display: none;\n        }\n\n        app-header {\n          color: #fff;\n          background-color: var(--app-primary-color);\n        }\n\n        app-header paper-icon-button {\n          --paper-icon-button-ink-color: white;\n        }\n\n        .drawer-list {\n          margin: 0 20px;\n        }\n\n        .drawer-list a {\n          display: block;\n          padding: 0 16px;\n          text-decoration: none;\n          color: var(--app-tertiary-color);\n          line-height: 40px;\n        }\n\n        .drawer-list a.iron-selected {\n          color: var(--app-tertiary-color);\n          font-weight: bold;\n        }\n      </style>\n\n      <app-location route=\"{{route}}\" url-space-regex=\"^[[rootPath]]\">\n      </app-location>\n\n      <app-route route=\"{{route}}\" pattern=\"[[rootPath]]:page\" data=\"{{routeData}}\" tail=\"{{subroute}}\">\n      </app-route>\n\n      <app-drawer-layout fullbleed=\"\" narrow=\"{{narrow}}\">\n        <!-- Drawer content -->\n        <app-drawer id=\"drawer\" slot=\"drawer\" swipe-open=\"[[narrow]]\">\n          <app-toolbar></app-toolbar>\n          <iron-selector selected=\"[[page]]\" attr-for-selected=\"name\" class=\"drawer-list\" role=\"navigation\">\n            <a name=\"view1\" href=\"[[rootPath]]view1\">Tendencia Financiera</a>\n            <a name=\"view2\" href=\"[[rootPath]]view2\">Clasificaci\xF3n de Gastos</a>\n            <a name=\"view3\" href=\"[[rootPath]]view3\">Adm\xF3n de Categor\xEDas</a>\n          </iron-selector>\n        </app-drawer>\n\n        <!-- Main content -->\n        <app-header-layout has-scrolling-region=\"\">\n\n          <app-header slot=\"header\" condenses=\"\" reveals=\"\" effects=\"waterfall\">\n            <app-toolbar>\n              <paper-icon-button icon=\"my-icons:menu\" drawer-toggle=\"\"></paper-icon-button>\n              <div main-title=\"\">TRENDS</div>\n            </app-toolbar>\n          </app-header>\n\n          <slot></slot>\n        </app-header-layout>\n      </app-drawer-layout>\n    "]);_templateObject5_ba974b809c2211eaa17be74ebc068880=function _templateObject5_ba974b809c2211eaa17be74ebc068880(){return data};return data}function _templateObject4_ba974b809c2211eaa17be74ebc068880(){var data=babelHelpers.taggedTemplateLiteral(["\n    <style>\n      :host {\n        position: relative;\n        display: block;\n        transition-timing-function: linear;\n        transition-property: -webkit-transform;\n        transition-property: transform;\n      }\n\n      :host::before {\n        position: absolute;\n        right: 0px;\n        bottom: -5px;\n        left: 0px;\n        width: 100%;\n        height: 5px;\n        content: \"\";\n        transition: opacity 0.4s;\n        pointer-events: none;\n        opacity: 0;\n        box-shadow: inset 0px 5px 6px -3px rgba(0, 0, 0, 0.4);\n        will-change: opacity;\n        @apply --app-header-shadow;\n      }\n\n      :host([shadow])::before {\n        opacity: 1;\n      }\n\n      #background {\n        @apply --layout-fit;\n        overflow: hidden;\n      }\n\n      #backgroundFrontLayer,\n      #backgroundRearLayer {\n        @apply --layout-fit;\n        height: 100%;\n        pointer-events: none;\n        background-size: cover;\n      }\n\n      #backgroundFrontLayer {\n        @apply --app-header-background-front-layer;\n      }\n\n      #backgroundRearLayer {\n        opacity: 0;\n        @apply --app-header-background-rear-layer;\n      }\n\n      #contentContainer {\n        position: relative;\n        width: 100%;\n        height: 100%;\n      }\n\n      :host([disabled]),\n      :host([disabled])::after,\n      :host([disabled]) #backgroundFrontLayer,\n      :host([disabled]) #backgroundRearLayer,\n      /* Silent scrolling should not run CSS transitions */\n      :host([silent-scroll]),\n      :host([silent-scroll])::after,\n      :host([silent-scroll]) #backgroundFrontLayer,\n      :host([silent-scroll]) #backgroundRearLayer {\n        transition: none !important;\n      }\n\n      :host([disabled]) ::slotted(app-toolbar:first-of-type),\n      :host([disabled]) ::slotted([sticky]),\n      /* Silent scrolling should not run CSS transitions */\n      :host([silent-scroll]) ::slotted(app-toolbar:first-of-type),\n      :host([silent-scroll]) ::slotted([sticky]) {\n        transition: none !important;\n      }\n\n    </style>\n    <div id=\"contentContainer\">\n      <slot id=\"slot\"></slot>\n    </div>\n"]);_templateObject4_ba974b809c2211eaa17be74ebc068880=function _templateObject4_ba974b809c2211eaa17be74ebc068880(){return data};return data}function _templateObject3_ba974b809c2211eaa17be74ebc068880(){var data=babelHelpers.taggedTemplateLiteral(["\n    <style>\n      :host {\n        display: block;\n        /**\n         * Force app-header-layout to have its own stacking context so that its parent can\n         * control the stacking of it relative to other elements (e.g. app-drawer-layout).\n         * This could be done using `isolation: isolate`, but that's not well supported\n         * across browsers.\n         */\n        position: relative;\n        z-index: 0;\n      }\n\n      #wrapper ::slotted([slot=header]) {\n        @apply --layout-fixed-top;\n        z-index: 1;\n      }\n\n      #wrapper.initializing ::slotted([slot=header]) {\n        position: relative;\n      }\n\n      :host([has-scrolling-region]) {\n        height: 100%;\n      }\n\n      :host([has-scrolling-region]) #wrapper ::slotted([slot=header]) {\n        position: absolute;\n      }\n\n      :host([has-scrolling-region]) #wrapper.initializing ::slotted([slot=header]) {\n        position: relative;\n      }\n\n      :host([has-scrolling-region]) #wrapper #contentContainer {\n        @apply --layout-fit;\n        overflow-y: auto;\n        -webkit-overflow-scrolling: touch;\n      }\n\n      :host([has-scrolling-region]) #wrapper.initializing #contentContainer {\n        position: relative;\n      }\n\n      :host([fullbleed]) {\n        @apply --layout-vertical;\n        @apply --layout-fit;\n      }\n\n      :host([fullbleed]) #wrapper,\n      :host([fullbleed]) #wrapper #contentContainer {\n        @apply --layout-vertical;\n        @apply --layout-flex;\n      }\n\n      #contentContainer {\n        /* Create a stacking context here so that all children appear below the header. */\n        position: relative;\n        z-index: 0;\n      }\n\n      @media print {\n        :host([has-scrolling-region]) #wrapper #contentContainer {\n          overflow-y: visible;\n        }\n      }\n\n    </style>\n\n    <div id=\"wrapper\" class=\"initializing\">\n      <slot id=\"headerSlot\" name=\"header\"></slot>\n\n      <div id=\"contentContainer\">\n        <slot></slot>\n      </div>\n    </div>\n"],["\n    <style>\n      :host {\n        display: block;\n        /**\n         * Force app-header-layout to have its own stacking context so that its parent can\n         * control the stacking of it relative to other elements (e.g. app-drawer-layout).\n         * This could be done using \\`isolation: isolate\\`, but that's not well supported\n         * across browsers.\n         */\n        position: relative;\n        z-index: 0;\n      }\n\n      #wrapper ::slotted([slot=header]) {\n        @apply --layout-fixed-top;\n        z-index: 1;\n      }\n\n      #wrapper.initializing ::slotted([slot=header]) {\n        position: relative;\n      }\n\n      :host([has-scrolling-region]) {\n        height: 100%;\n      }\n\n      :host([has-scrolling-region]) #wrapper ::slotted([slot=header]) {\n        position: absolute;\n      }\n\n      :host([has-scrolling-region]) #wrapper.initializing ::slotted([slot=header]) {\n        position: relative;\n      }\n\n      :host([has-scrolling-region]) #wrapper #contentContainer {\n        @apply --layout-fit;\n        overflow-y: auto;\n        -webkit-overflow-scrolling: touch;\n      }\n\n      :host([has-scrolling-region]) #wrapper.initializing #contentContainer {\n        position: relative;\n      }\n\n      :host([fullbleed]) {\n        @apply --layout-vertical;\n        @apply --layout-fit;\n      }\n\n      :host([fullbleed]) #wrapper,\n      :host([fullbleed]) #wrapper #contentContainer {\n        @apply --layout-vertical;\n        @apply --layout-flex;\n      }\n\n      #contentContainer {\n        /* Create a stacking context here so that all children appear below the header. */\n        position: relative;\n        z-index: 0;\n      }\n\n      @media print {\n        :host([has-scrolling-region]) #wrapper #contentContainer {\n          overflow-y: visible;\n        }\n      }\n\n    </style>\n\n    <div id=\"wrapper\" class=\"initializing\">\n      <slot id=\"headerSlot\" name=\"header\"></slot>\n\n      <div id=\"contentContainer\">\n        <slot></slot>\n      </div>\n    </div>\n"]);_templateObject3_ba974b809c2211eaa17be74ebc068880=function _templateObject3_ba974b809c2211eaa17be74ebc068880(){return data};return data}function _templateObject2_ba974b809c2211eaa17be74ebc068880(){var data=babelHelpers.taggedTemplateLiteral(["\n    <style>\n      :host {\n        position: fixed;\n        top: -120px;\n        right: 0;\n        bottom: -120px;\n        left: 0;\n\n        visibility: hidden;\n\n        transition-property: visibility;\n      }\n\n      :host([opened]) {\n        visibility: visible;\n      }\n\n      :host([persistent]) {\n        width: var(--app-drawer-width, 256px);\n      }\n\n      :host([persistent][position=left]) {\n        right: auto;\n      }\n\n      :host([persistent][position=right]) {\n        left: auto;\n      }\n\n      #contentContainer {\n        position: absolute;\n        top: 0;\n        bottom: 0;\n        left: 0;\n\n        width: var(--app-drawer-width, 256px);\n        padding: var(--app-drawer-content-padding, 120px 0);\n\n        transition-property: -webkit-transform;\n        transition-property: transform;\n        -webkit-transform: translate3d(-100%, 0, 0);\n        transform: translate3d(-100%, 0, 0);\n\n        background-color: #FFF;\n\n        @apply --app-drawer-content-container;\n      }\n\n      #contentContainer[persistent] {\n        width: 100%;\n      }\n\n      #contentContainer[position=right] {\n        right: 0;\n        left: auto;\n\n        -webkit-transform: translate3d(100%, 0, 0);\n        transform: translate3d(100%, 0, 0);\n      }\n\n      #contentContainer[swipe-open]::after {\n        position: fixed;\n        top: 0;\n        bottom: 0;\n        left: 100%;\n\n        visibility: visible;\n\n        width: 20px;\n\n        content: '';\n      }\n\n      #contentContainer[swipe-open][position=right]::after {\n        right: 100%;\n        left: auto;\n      }\n\n      #contentContainer[opened] {\n        -webkit-transform: translate3d(0, 0, 0);\n        transform: translate3d(0, 0, 0);\n      }\n\n      #scrim {\n        position: absolute;\n        top: 0;\n        right: 0;\n        bottom: 0;\n        left: 0;\n\n        transition-property: opacity;\n        -webkit-transform: translateZ(0);\n        transform:  translateZ(0);\n\n        opacity: 0;\n        background: var(--app-drawer-scrim-background, rgba(0, 0, 0, 0.5));\n      }\n\n      #scrim.visible {\n        opacity: 1;\n      }\n\n      :host([no-transition]) #contentContainer {\n        transition-property: none;\n      }\n    </style>\n\n    <div id=\"scrim\" on-click=\"close\"></div>\n\n    <!-- HACK(keanulee): Bind attributes here (in addition to :host) for styling to workaround Safari\n    bug. https://bugs.webkit.org/show_bug.cgi?id=170762 -->\n    <div id=\"contentContainer\" opened$=\"[[opened]]\" persistent$=\"[[persistent]]\" position$=\"[[position]]\" swipe-open$=\"[[swipeOpen]]\">\n      <slot></slot>\n    </div>\n"],["\n    <style>\n      :host {\n        position: fixed;\n        top: -120px;\n        right: 0;\n        bottom: -120px;\n        left: 0;\n\n        visibility: hidden;\n\n        transition-property: visibility;\n      }\n\n      :host([opened]) {\n        visibility: visible;\n      }\n\n      :host([persistent]) {\n        width: var(--app-drawer-width, 256px);\n      }\n\n      :host([persistent][position=left]) {\n        right: auto;\n      }\n\n      :host([persistent][position=right]) {\n        left: auto;\n      }\n\n      #contentContainer {\n        position: absolute;\n        top: 0;\n        bottom: 0;\n        left: 0;\n\n        width: var(--app-drawer-width, 256px);\n        padding: var(--app-drawer-content-padding, 120px 0);\n\n        transition-property: -webkit-transform;\n        transition-property: transform;\n        -webkit-transform: translate3d(-100%, 0, 0);\n        transform: translate3d(-100%, 0, 0);\n\n        background-color: #FFF;\n\n        @apply --app-drawer-content-container;\n      }\n\n      #contentContainer[persistent] {\n        width: 100%;\n      }\n\n      #contentContainer[position=right] {\n        right: 0;\n        left: auto;\n\n        -webkit-transform: translate3d(100%, 0, 0);\n        transform: translate3d(100%, 0, 0);\n      }\n\n      #contentContainer[swipe-open]::after {\n        position: fixed;\n        top: 0;\n        bottom: 0;\n        left: 100%;\n\n        visibility: visible;\n\n        width: 20px;\n\n        content: '';\n      }\n\n      #contentContainer[swipe-open][position=right]::after {\n        right: 100%;\n        left: auto;\n      }\n\n      #contentContainer[opened] {\n        -webkit-transform: translate3d(0, 0, 0);\n        transform: translate3d(0, 0, 0);\n      }\n\n      #scrim {\n        position: absolute;\n        top: 0;\n        right: 0;\n        bottom: 0;\n        left: 0;\n\n        transition-property: opacity;\n        -webkit-transform: translateZ(0);\n        transform:  translateZ(0);\n\n        opacity: 0;\n        background: var(--app-drawer-scrim-background, rgba(0, 0, 0, 0.5));\n      }\n\n      #scrim.visible {\n        opacity: 1;\n      }\n\n      :host([no-transition]) #contentContainer {\n        transition-property: none;\n      }\n    </style>\n\n    <div id=\"scrim\" on-click=\"close\"></div>\n\n    <!-- HACK(keanulee): Bind attributes here (in addition to :host) for styling to workaround Safari\n    bug. https://bugs.webkit.org/show_bug.cgi?id=170762 -->\n    <div id=\"contentContainer\" opened\\$=\"[[opened]]\" persistent\\$=\"[[persistent]]\" position\\$=\"[[position]]\" swipe-open\\$=\"[[swipeOpen]]\">\n      <slot></slot>\n    </div>\n"]);_templateObject2_ba974b809c2211eaa17be74ebc068880=function _templateObject2_ba974b809c2211eaa17be74ebc068880(){return data};return data}function _templateObject_ba974b809c2211eaa17be74ebc068880(){var data=babelHelpers.taggedTemplateLiteral(["\n    <style>\n      :host {\n        display: block;\n        /**\n         * Force app-drawer-layout to have its own stacking context so that its parent can\n         * control the stacking of it relative to other elements.\n         */\n        position: relative;\n        z-index: 0;\n      }\n\n      :host ::slotted([slot=drawer]) {\n        z-index: 1;\n      }\n\n      :host([fullbleed]) {\n        @apply --layout-fit;\n      }\n\n      #contentContainer {\n        /* Create a stacking context here so that all children appear below the header. */\n        position: relative;\n        z-index: 0;\n        height: 100%;\n        transition: var(--app-drawer-layout-content-transition, none);\n      }\n\n      #contentContainer[drawer-position=left] {\n        margin-left: var(--app-drawer-width, 256px);\n      }\n\n      #contentContainer[drawer-position=right] {\n        margin-right: var(--app-drawer-width, 256px);\n      }\n    </style>\n\n    <slot id=\"drawerSlot\" name=\"drawer\"></slot>\n\n    <div id=\"contentContainer\" drawer-position$=\"[[_drawerPosition]]\">\n      <slot></slot>\n    </div>\n\n    <iron-media-query query=\"[[_computeMediaQuery(forceNarrow, responsiveWidth)]]\" on-query-matches-changed=\"_onQueryMatchesChanged\"></iron-media-query>\n"],["\n    <style>\n      :host {\n        display: block;\n        /**\n         * Force app-drawer-layout to have its own stacking context so that its parent can\n         * control the stacking of it relative to other elements.\n         */\n        position: relative;\n        z-index: 0;\n      }\n\n      :host ::slotted([slot=drawer]) {\n        z-index: 1;\n      }\n\n      :host([fullbleed]) {\n        @apply --layout-fit;\n      }\n\n      #contentContainer {\n        /* Create a stacking context here so that all children appear below the header. */\n        position: relative;\n        z-index: 0;\n        height: 100%;\n        transition: var(--app-drawer-layout-content-transition, none);\n      }\n\n      #contentContainer[drawer-position=left] {\n        margin-left: var(--app-drawer-width, 256px);\n      }\n\n      #contentContainer[drawer-position=right] {\n        margin-right: var(--app-drawer-width, 256px);\n      }\n    </style>\n\n    <slot id=\"drawerSlot\" name=\"drawer\"></slot>\n\n    <div id=\"contentContainer\" drawer-position\\$=\"[[_drawerPosition]]\">\n      <slot></slot>\n    </div>\n\n    <iron-media-query query=\"[[_computeMediaQuery(forceNarrow, responsiveWidth)]]\" on-query-matches-changed=\"_onQueryMatchesChanged\"></iron-media-query>\n"]);_templateObject_ba974b809c2211eaa17be74ebc068880=function _templateObject_ba974b809c2211eaa17be74ebc068880(){return data};return data}(0,_myAppnav.Polymer)({is:"iron-media-query",properties:{/**
     * The Boolean return value of the media query.
     */queryMatches:{type:Boolean,value:!1,readOnly:!0,notify:!0},/**
     * The CSS media query to evaluate.
     */query:{type:String,observer:"queryChanged"},/**
     * If true, the query attribute is assumed to be a complete media query
     * string rather than a single media feature.
     */full:{type:Boolean,value:!1},/**
     * @type {function(MediaQueryList)}
     */_boundMQHandler:{value:function value(){return this.queryHandler.bind(this)}},/**
     * @type {MediaQueryList}
     */_mq:{value:null}},attached:function attached(){this.style.display="none";this.queryChanged()},detached:function detached(){this._remove()},_add:function _add(){if(this._mq){this._mq.addListener(this._boundMQHandler)}},_remove:function _remove(){if(this._mq){this._mq.removeListener(this._boundMQHandler)}this._mq=null},queryChanged:function queryChanged(){this._remove();var query=this.query;if(!query){return}if(!this.full&&"("!==query[0]){query="("+query+")"}this._mq=window.matchMedia(query);this._add();this.queryHandler(this._mq)},queryHandler:function queryHandler(mq){this._setQueryMatches(mq.matches)}});var AppLayoutBehavior=[_myAppnav.IronResizableBehavior,{listeners:{"app-reset-layout":"_appResetLayoutHandler","iron-resize":"resetLayout"},attached:function attached(){this.fire("app-reset-layout")},_appResetLayoutHandler:function _appResetLayoutHandler(e){if((0,_myAppnav.dom)(e).path[0]===this){return}this.resetLayout();e.stopPropagation()},_updateLayoutStates:function _updateLayoutStates(){console.error("unimplemented")},/**
   * Resets the layout. If you changed the size of this element via CSS
   * you can notify the changes by either firing the `iron-resize` event
   * or calling `resetLayout` directly.
   *
   * @method resetLayout
   */resetLayout:function resetLayout(){var self=this,cb=this._updateLayoutStates.bind(this);this._layoutDebouncer=_myAppnav.Debouncer.debounce(this._layoutDebouncer,_myAppnav.animationFrame,cb);(0,_myAppnav.enqueueDebouncer)(this._layoutDebouncer);this._notifyDescendantResize()},_notifyLayoutChanged:function _notifyLayoutChanged(){var self=this;// TODO: the event `app-reset-layout` can be fired synchronously
// as long as `_updateLayoutStates` waits for all the microtasks after
// rAF. E.g. requestAnimationFrame(setTimeOut())
requestAnimationFrame(function(){self.fire("app-reset-layout")})},_notifyDescendantResize:function _notifyDescendantResize(){if(!this.isAttached){return}this._interestedResizables.forEach(function(resizable){if(this.resizerShouldNotify(resizable)){this._notifyDescendant(resizable)}},this)}}];_exports.AppLayoutBehavior=AppLayoutBehavior;var appLayoutBehavior={AppLayoutBehavior:AppLayoutBehavior};_exports.$appLayoutBehavior=appLayoutBehavior;(0,_myAppnav.Polymer)({/** @override */_template:(0,_myAppnav.html)(_templateObject_ba974b809c2211eaa17be74ebc068880()),is:"app-drawer-layout",behaviors:[AppLayoutBehavior],properties:{/**
     * If true, ignore `responsiveWidth` setting and force the narrow layout.
     */forceNarrow:{type:Boolean,value:!1},/**
     * If the viewport's width is smaller than this value, the panel will change
     * to narrow layout. In the mode the drawer will be closed.
     */responsiveWidth:{type:String,value:"640px"},/**
     * Returns true if it is in narrow layout. This is useful if you need to
     * show/hide elements based on the layout.
     */narrow:{type:Boolean,reflectToAttribute:!0,readOnly:!0,notify:!0},/**
     * If true, the drawer will initially be opened when in narrow layout mode.
     */openedWhenNarrow:{type:Boolean,value:!1},_drawerPosition:{type:String}},listeners:{click:"_clickHandler"},observers:["_narrowChanged(narrow)"],/**
   * A reference to the app-drawer element.
   *
   * @property drawer
   */get drawer(){return(0,_myAppnav.dom)(this.$.drawerSlot).getDistributedNodes()[0]},/** @override */attached:function attached(){// Disable drawer transitions until after app-drawer-layout sets the initial
// opened state.
var drawer=this.drawer;if(drawer){drawer.setAttribute("no-transition","")}},_clickHandler:function _clickHandler(e){var target=(0,_myAppnav.dom)(e).localTarget;if(target&&target.hasAttribute("drawer-toggle")){var drawer=this.drawer;if(drawer&&!drawer.persistent){drawer.toggle()}}},_updateLayoutStates:function _updateLayoutStates(){var drawer=this.drawer;if(!this.isAttached||!drawer){return}this._drawerPosition=this.narrow?null:drawer.position;if(this._drawerNeedsReset){if(this.narrow){drawer.opened=this.openedWhenNarrow;drawer.persistent=!1}else{drawer.opened=drawer.persistent=!0}if(drawer.hasAttribute("no-transition")){// Enable drawer transitions after app-drawer-layout sets the initial
// opened state.
(0,_myAppnav.afterNextRender)(this,function(){drawer.removeAttribute("no-transition")})}this._drawerNeedsReset=!1}},_narrowChanged:function _narrowChanged(){this._drawerNeedsReset=!0;this.resetLayout()},_onQueryMatchesChanged:function _onQueryMatchesChanged(event){this._setNarrow(event.detail.value)},_computeMediaQuery:function _computeMediaQuery(forceNarrow,responsiveWidth){return forceNarrow?"(min-width: 0px)":"(max-width: "+responsiveWidth+")"}});(0,_myAppnav.Polymer)({/** @override */_template:(0,_myAppnav.html)(_templateObject2_ba974b809c2211eaa17be74ebc068880()),is:"app-drawer",properties:{/**
     * The opened state of the drawer.
     */opened:{type:Boolean,value:!1,notify:!0,reflectToAttribute:!0},/**
     * The drawer does not have a scrim and cannot be swiped close.
     */persistent:{type:Boolean,value:!1,reflectToAttribute:!0},/**
     * The transition duration of the drawer in milliseconds.
     */transitionDuration:{type:Number,value:200},/**
     * The alignment of the drawer on the screen ('left', 'right', 'start' or
     * 'end'). 'start' computes to left and 'end' to right in LTR layout and
     * vice versa in RTL layout.
     */align:{type:String,value:"left"},/**
     * The computed, read-only position of the drawer on the screen ('left' or
     * 'right').
     */position:{type:String,readOnly:!0,reflectToAttribute:!0},/**
     * Create an area at the edge of the screen to swipe open the drawer.
     */swipeOpen:{type:Boolean,value:!1,reflectToAttribute:!0},/**
     * Trap keyboard focus when the drawer is opened and not persistent.
     */noFocusTrap:{type:Boolean,value:!1},/**
     * Disables swiping on the drawer.
     */disableSwipe:{type:Boolean,value:!1}},observers:["resetLayout(position, isAttached)","_resetPosition(align, isAttached)","_styleTransitionDuration(transitionDuration)","_openedPersistentChanged(opened, persistent)"],_translateOffset:0,_trackDetails:null,_drawerState:0,_boundEscKeydownHandler:null,_firstTabStop:null,_lastTabStop:null,/** @override */attached:function attached(){(0,_myAppnav.afterNextRender)(this,function(){this._boundEscKeydownHandler=this._escKeydownHandler.bind(this);this.addEventListener("keydown",this._tabKeydownHandler.bind(this));// Only listen for horizontal track so you can vertically scroll
// inside the drawer.
this.listen(this,"track","_track");this.setScrollDirection("y")});this.fire("app-reset-layout")},/** @override */detached:function detached(){document.removeEventListener("keydown",this._boundEscKeydownHandler)},/**
   * Opens the drawer.
   */open:function open(){this.opened=!0},/**
   * Closes the drawer.
   */close:function close(){this.opened=!1},/**
   * Toggles the drawer open and close.
   */toggle:function toggle(){this.opened=!this.opened},/**
   * Gets the width of the drawer.
   *
   * @return {number} The width of the drawer in pixels.
   */getWidth:function getWidth(){return this._savedWidth||this.$.contentContainer.offsetWidth},_isRTL:function _isRTL(){return"rtl"===window.getComputedStyle(this).direction},_resetPosition:function _resetPosition(){switch(this.align){case"start":this._setPosition(this._isRTL()?"right":"left");return;case"end":this._setPosition(this._isRTL()?"left":"right");return;}this._setPosition(this.align)},_escKeydownHandler:function _escKeydownHandler(event){var ESC_KEYCODE=27;if(event.keyCode===ESC_KEYCODE){// Prevent any side effects if app-drawer closes.
event.preventDefault();this.close()}},_track:function _track(event){if(this.persistent||this.disableSwipe){return}// Disable user selection on desktop.
event.preventDefault();switch(event.detail.state){case"start":this._trackStart(event);break;case"track":this._trackMove(event);break;case"end":this._trackEnd(event);break;}},_trackStart:function _trackStart(event){this._drawerState=this._DRAWER_STATE.TRACKING;var rect=this.$.contentContainer.getBoundingClientRect();this._savedWidth=rect.width;if("left"===this.position){this._translateOffset=rect.left}else{this._translateOffset=rect.right-window.innerWidth}this._trackDetails=[];// Disable transitions since style attributes will reflect user track
// events.
this._styleTransitionDuration(0);this.style.visibility="visible"},_trackMove:function _trackMove(event){this._translateDrawer(event.detail.dx+this._translateOffset);// Use Date.now() since event.timeStamp is inconsistent across browsers
// (e.g. most browsers use milliseconds but FF 44 uses microseconds).
this._trackDetails.push({dx:event.detail.dx,timeStamp:Date.now()})},_trackEnd:function _trackEnd(event){var x=event.detail.dx+this._translateOffset,drawerWidth=this.getWidth(),isPositionLeft="left"===this.position,isInEndState=isPositionLeft?0<=x||x<=-drawerWidth:0>=x||x>=drawerWidth;if(!isInEndState){// No longer need the track events after this method returns - allow them
// to be GC'd.
var trackDetails=this._trackDetails;this._trackDetails=null;this._flingDrawer(event,trackDetails);if(this._drawerState===this._DRAWER_STATE.FLINGING){return}}// If the drawer is not flinging, toggle the opened state based on the
// position of the drawer.
var halfWidth=drawerWidth/2;if(event.detail.dx<-halfWidth){this.opened="right"===this.position}else if(event.detail.dx>halfWidth){this.opened="left"===this.position}if(isInEndState){this.debounce("_resetDrawerState",this._resetDrawerState)}else{this.debounce("_resetDrawerState",this._resetDrawerState,this.transitionDuration)}this._styleTransitionDuration(this.transitionDuration);this._resetDrawerTranslate();this.style.visibility=""},_calculateVelocity:function _calculateVelocity(event,trackDetails){// Find the oldest track event that is within 100ms using binary search.
var now=Date.now(),timeLowerBound=now-100,trackDetail,min=0,max=trackDetails.length-1;while(min<=max){// Floor of average of min and max.
var mid=min+max>>1,d=trackDetails[mid];if(d.timeStamp>=timeLowerBound){trackDetail=d;max=mid-1}else{min=mid+1}}if(trackDetail){var dx=event.detail.dx-trackDetail.dx,dt=now-trackDetail.timeStamp||1;return dx/dt}return 0},_flingDrawer:function _flingDrawer(event,trackDetails){var velocity=this._calculateVelocity(event,trackDetails);// Do not fling if velocity is not above a threshold.
if(Math.abs(velocity)<this._MIN_FLING_THRESHOLD){return}this._drawerState=this._DRAWER_STATE.FLINGING;var x=event.detail.dx+this._translateOffset,drawerWidth=this.getWidth(),isPositionLeft="left"===this.position,isVelocityPositive=0<velocity,isClosingLeft=!isVelocityPositive&&isPositionLeft,isClosingRight=isVelocityPositive&&!isPositionLeft,dx;if(isClosingLeft){dx=-(x+drawerWidth)}else if(isClosingRight){dx=drawerWidth-x}else{dx=-x}// Enforce a minimum transition velocity to make the drawer feel snappy.
if(isVelocityPositive){velocity=Math.max(velocity,this._MIN_TRANSITION_VELOCITY);this.opened="left"===this.position}else{velocity=Math.min(velocity,-this._MIN_TRANSITION_VELOCITY);this.opened="right"===this.position}// Calculate the amount of time needed to finish the transition based on the
// initial slope of the timing function.
var t=this._FLING_INITIAL_SLOPE*dx/velocity;this._styleTransitionDuration(t);this._styleTransitionTimingFunction(this._FLING_TIMING_FUNCTION);this._resetDrawerTranslate();this.debounce("_resetDrawerState",this._resetDrawerState,t)},_styleTransitionDuration:function _styleTransitionDuration(duration){this.style.transitionDuration=duration+"ms";this.$.contentContainer.style.transitionDuration=duration+"ms";this.$.scrim.style.transitionDuration=duration+"ms"},_styleTransitionTimingFunction:function _styleTransitionTimingFunction(timingFunction){this.$.contentContainer.style.transitionTimingFunction=timingFunction;this.$.scrim.style.transitionTimingFunction=timingFunction},_translateDrawer:function _translateDrawer(x){var drawerWidth=this.getWidth();if("left"===this.position){x=Math.max(-drawerWidth,Math.min(x,0));this.$.scrim.style.opacity=1+x/drawerWidth}else{x=Math.max(0,Math.min(x,drawerWidth));this.$.scrim.style.opacity=1-x/drawerWidth}this.translate3d(x+"px","0","0",this.$.contentContainer)},_resetDrawerTranslate:function _resetDrawerTranslate(){this.$.scrim.style.opacity="";this.transform("",this.$.contentContainer)},_resetDrawerState:function _resetDrawerState(){var oldState=this._drawerState;// If the drawer was flinging, we need to reset the style attributes.
if(oldState===this._DRAWER_STATE.FLINGING){this._styleTransitionDuration(this.transitionDuration);this._styleTransitionTimingFunction("");this.style.visibility=""}this._savedWidth=null;if(this.opened){this._drawerState=this.persistent?this._DRAWER_STATE.OPENED_PERSISTENT:this._DRAWER_STATE.OPENED}else{this._drawerState=this._DRAWER_STATE.CLOSED}if(oldState!==this._drawerState){if(this._drawerState===this._DRAWER_STATE.OPENED){this._setKeyboardFocusTrap();document.addEventListener("keydown",this._boundEscKeydownHandler);document.body.style.overflow="hidden"}else{document.removeEventListener("keydown",this._boundEscKeydownHandler);document.body.style.overflow=""}// Don't fire the event on initial load.
if(oldState!==this._DRAWER_STATE.INIT){this.fire("app-drawer-transitioned")}}},/**
   * Resets the layout.
   *
   * @method resetLayout
   */resetLayout:function resetLayout(){this.fire("app-reset-layout")},_setKeyboardFocusTrap:function _setKeyboardFocusTrap(){if(this.noFocusTrap){return}// NOTE: Unless we use /deep/ (which we shouldn't since it's deprecated),
// this will not select focusable elements inside shadow roots.
var focusableElementsSelector=["a[href]:not([tabindex=\"-1\"])","area[href]:not([tabindex=\"-1\"])","input:not([disabled]):not([tabindex=\"-1\"])","select:not([disabled]):not([tabindex=\"-1\"])","textarea:not([disabled]):not([tabindex=\"-1\"])","button:not([disabled]):not([tabindex=\"-1\"])","iframe:not([tabindex=\"-1\"])","[tabindex]:not([tabindex=\"-1\"])","[contentEditable=true]:not([tabindex=\"-1\"])"].join(","),focusableElements=(0,_myAppnav.dom)(this).querySelectorAll(focusableElementsSelector);if(0<focusableElements.length){this._firstTabStop=focusableElements[0];this._lastTabStop=focusableElements[focusableElements.length-1]}else{// Reset saved tab stops when there are no focusable elements in the
// drawer.
this._firstTabStop=null;this._lastTabStop=null}// Focus on app-drawer if it has non-zero tabindex. Otherwise, focus the
// first focusable element in the drawer, if it exists. Use the tabindex
// attribute since the this.tabIndex property in IE/Edge returns 0 (instead
// of -1) when the attribute is not set.
var tabindex=this.getAttribute("tabindex");if(tabindex&&-1<parseInt(tabindex,10)){this.focus()}else if(this._firstTabStop){this._firstTabStop.focus()}},_tabKeydownHandler:function _tabKeydownHandler(event){if(this.noFocusTrap){return}var TAB_KEYCODE=9;if(this._drawerState===this._DRAWER_STATE.OPENED&&event.keyCode===TAB_KEYCODE){if(event.shiftKey){if(this._firstTabStop&&(0,_myAppnav.dom)(event).localTarget===this._firstTabStop){event.preventDefault();this._lastTabStop.focus()}}else{if(this._lastTabStop&&(0,_myAppnav.dom)(event).localTarget===this._lastTabStop){event.preventDefault();this._firstTabStop.focus()}}}},_openedPersistentChanged:function _openedPersistentChanged(opened,persistent){this.toggleClass("visible",opened&&!persistent,this.$.scrim);// Use a debounce timer instead of transitionend since transitionend won't
// fire when app-drawer is display: none.
this.debounce("_resetDrawerState",this._resetDrawerState,this.transitionDuration)},_MIN_FLING_THRESHOLD:.2,_MIN_TRANSITION_VELOCITY:1.2,_FLING_TIMING_FUNCTION:"cubic-bezier(0.667, 1, 0.667, 1)",_FLING_INITIAL_SLOPE:1.5,_DRAWER_STATE:{INIT:0,OPENED:1,OPENED_PERSISTENT:2,CLOSED:3,TRACKING:4,FLINGING:5/**
                 * Fired when the layout of app-drawer has changed.
                 *
                 * @event app-reset-layout
                 */ /**
                     * Fired when app-drawer has finished transitioning.
                     *
                     * @event app-drawer-transitioned
                     */}});(0,_myAppnav.Polymer)({/** @override */_template:(0,_myAppnav.html)(_templateObject3_ba974b809c2211eaa17be74ebc068880()),is:"app-header-layout",behaviors:[AppLayoutBehavior],properties:{/**
     * If true, the current element will have its own scrolling region.
     * Otherwise, it will use the document scroll to control the header.
     */hasScrollingRegion:{type:Boolean,value:!1,reflectToAttribute:!0}},observers:["resetLayout(isAttached, hasScrollingRegion)"],/**
   * A reference to the app-header element.
   *
   * @property header
   */get header(){return(0,_myAppnav.dom)(this.$.headerSlot).getDistributedNodes()[0]},_updateLayoutStates:function _updateLayoutStates(){var header=this.header;if(!this.isAttached||!header){return}// Remove the initializing class, which staticly positions the header and
// the content until the height of the header can be read.
this.$.wrapper.classList.remove("initializing");// Update scroll target.
header.scrollTarget=this.hasScrollingRegion?this.$.contentContainer:this.ownerDocument.documentElement;// Get header height here so that style reads are batched together before
// style writes (i.e. getBoundingClientRect() below).
var headerHeight=header.offsetHeight;// Update the header position.
if(!this.hasScrollingRegion){requestAnimationFrame(function(){var rect=this.getBoundingClientRect(),rightOffset=document.documentElement.clientWidth-rect.right;header.style.left=rect.left+"px";header.style.right=rightOffset+"px"}.bind(this))}else{header.style.left="";header.style.right=""}// Update the content container position.
var containerStyle=this.$.contentContainer.style;if(header.fixed&&!header.condenses&&this.hasScrollingRegion){// If the header size does not change and we're using a scrolling region,
// exclude the header area from the scrolling region so that the header
// doesn't overlap the scrollbar.
containerStyle.marginTop=headerHeight+"px";containerStyle.paddingTop=""}else{containerStyle.paddingTop=headerHeight+"px";containerStyle.marginTop=""}}});(0,_myAppnav.Polymer)({/** @override */_template:(0,_myAppnav.html)(_templateObject4_ba974b809c2211eaa17be74ebc068880()),is:"app-header",behaviors:[_myAppnav.AppScrollEffectsBehavior,AppLayoutBehavior],properties:{/**
     * If true, the header will automatically collapse when scrolling down.
     * That is, the `sticky` element remains visible when the header is fully
     *condensed whereas the rest of the elements will collapse below `sticky`
     *element.
     *
     * By default, the `sticky` element is the first toolbar in the light DOM:
     *
     *```html
     * <app-header condenses>
     *   <app-toolbar>This toolbar remains on top</app-toolbar>
     *   <app-toolbar></app-toolbar>
     *   <app-toolbar></app-toolbar>
     * </app-header>
     * ```
     *
     * Additionally, you can specify which toolbar or element remains visible in
     *condensed mode by adding the `sticky` attribute to that element. For
     *example: if we want the last toolbar to remain visible, we can add the
     *`sticky` attribute to it.
     *
     *```html
     * <app-header condenses>
     *   <app-toolbar></app-toolbar>
     *   <app-toolbar></app-toolbar>
     *   <app-toolbar sticky>This toolbar remains on top</app-toolbar>
     * </app-header>
     * ```
     *
     * Note the `sticky` element must be a direct child of `app-header`.
     */condenses:{type:Boolean,value:!1},/**
     * Mantains the header fixed at the top so it never moves away.
     */fixed:{type:Boolean,value:!1},/**
     * Slides back the header when scrolling back up.
     */reveals:{type:Boolean,value:!1},/**
     * Displays a shadow below the header.
     */shadow:{type:Boolean,reflectToAttribute:!0,value:!1}},observers:["_configChanged(isAttached, condenses, fixed)"],/**
   * A cached offsetHeight of the current element.
   *
   * @type {number}
   */_height:0,/**
   * The distance in pixels the header will be translated to when scrolling.
   *
   * @type {number}
   */_dHeight:0,/**
   * The offsetTop of `_stickyEl`
   *
   * @type {number}
   */_stickyElTop:0,/**
   * A reference to the element that remains visible when the header condenses.
   *
   * @type {HTMLElement}
   */_stickyElRef:null,/**
   * The header's top value used for the `transformY`
   *
   * @type {number}
   */_top:0,/**
   * The current scroll progress.
   *
   * @type {number}
   */_progress:0,_wasScrollingDown:!1,_initScrollTop:0,_initTimestamp:0,_lastTimestamp:0,_lastScrollTop:0,/**
   * The distance the header is allowed to move away.
   *
   * @type {number}
   */get _maxHeaderTop(){return this.fixed?this._dHeight:this._height+5},/**
   * Returns a reference to the sticky element.
   *
   * @return {HTMLElement}?
   */get _stickyEl(){if(this._stickyElRef){return this._stickyElRef}// Get the element with the sticky attribute on it or the first element in
// the light DOM.
for(var nodes=(0,_myAppnav.dom)(this.$.slot).getDistributedNodes(),i=0,node;node=/** @type {!HTMLElement} */nodes[i];i++){if(node.nodeType===Node.ELEMENT_NODE){if(node.hasAttribute("sticky")){this._stickyElRef=node;break}else if(!this._stickyElRef){this._stickyElRef=node}}}return this._stickyElRef},_configChanged:function _configChanged(){this.resetLayout();this._notifyLayoutChanged()},_updateLayoutStates:function _updateLayoutStates(){if(0===this.offsetWidth&&0===this.offsetHeight){return}var scrollTop=this._clampedScrollTop,firstSetup=0===this._height||0===scrollTop,currentDisabled=this.disabled;this._height=this.offsetHeight;this._stickyElRef=null;this.disabled=!0;// prepare for measurement
if(!firstSetup){this._updateScrollState(0,!0)}if(this._mayMove()){this._dHeight=this._stickyEl?this._height-this._stickyEl.offsetHeight:0}else{this._dHeight=0}this._stickyElTop=this._stickyEl?this._stickyEl.offsetTop:0;this._setUpEffect();if(firstSetup){this._updateScrollState(scrollTop,!0)}else{this._updateScrollState(this._lastScrollTop,!0);this._layoutIfDirty()}// restore no transition
this.disabled=currentDisabled},/**
   * Updates the scroll state.
   *
   * @param {number} scrollTop
   * @param {boolean=} forceUpdate (default: false)
   */_updateScrollState:function _updateScrollState(scrollTop,forceUpdate){if(0===this._height){return}var progress=0,top=0,lastTop=this._top,lastScrollTop=this._lastScrollTop,maxHeaderTop=this._maxHeaderTop,dScrollTop=scrollTop-this._lastScrollTop,absDScrollTop=Math.abs(dScrollTop),isScrollingDown=scrollTop>this._lastScrollTop,now=performance.now();if(this._mayMove()){top=this._clamp(this.reveals?lastTop+dScrollTop:scrollTop,0,maxHeaderTop)}if(scrollTop>=this._dHeight){top=this.condenses&&!this.fixed?Math.max(this._dHeight,top):top;this.style.transitionDuration="0ms"}if(this.reveals&&!this.disabled&&100>absDScrollTop){// set the initial scroll position
if(300<now-this._initTimestamp||this._wasScrollingDown!==isScrollingDown){this._initScrollTop=scrollTop;this._initTimestamp=now}if(scrollTop>=maxHeaderTop){// check if the header is allowed to snap
if(30<Math.abs(this._initScrollTop-scrollTop)||10<absDScrollTop){if(isScrollingDown&&scrollTop>=maxHeaderTop){top=maxHeaderTop}else if(!isScrollingDown&&scrollTop>=this._dHeight){top=this.condenses&&!this.fixed?this._dHeight:0}var scrollVelocity=dScrollTop/(now-this._lastTimestamp);this.style.transitionDuration=this._clamp((top-lastTop)/scrollVelocity,0,300)+"ms"}else{top=this._top}}}if(0===this._dHeight){progress=0<scrollTop?1:0}else{progress=top/this._dHeight}if(!forceUpdate){this._lastScrollTop=scrollTop;this._top=top;this._wasScrollingDown=isScrollingDown;this._lastTimestamp=now}if(forceUpdate||progress!==this._progress||lastTop!==top||0===scrollTop){this._progress=progress;this._runEffects(progress,top);this._transformHeader(top)}},/**
   * Returns true if the current header is allowed to move as the user scrolls.
   *
   * @return {boolean}
   */_mayMove:function _mayMove(){return this.condenses||!this.fixed},/**
   * Returns true if the current header will condense based on the size of the
   * header and the `consenses` property.
   *
   * @return {boolean}
   */willCondense:function willCondense(){return 0<this._dHeight&&this.condenses},/**
   * Returns true if the current element is on the screen.
   * That is, visible in the current viewport.
   *
   * @method isOnScreen
   * @return {boolean}
   */isOnScreen:function isOnScreen(){return 0!==this._height&&this._top<this._height},/**
   * Returns true if there's content below the current element.
   *
   * @method isContentBelow
   * @return {boolean}
   */isContentBelow:function isContentBelow(){return 0===this._top?0<this._clampedScrollTop:0<=this._clampedScrollTop-this._maxHeaderTop},/**
   * Transforms the header.
   *
   * @param {number} y
   */_transformHeader:function _transformHeader(y){this.translate3d(0,-y+"px",0);if(this._stickyEl){this.translate3d(0,this.condenses&&y>=this._stickyElTop?Math.min(y,this._dHeight)-this._stickyElTop+"px":0,0,this._stickyEl)}},_clamp:function _clamp(v,min,max){return Math.min(max,Math.max(min,v))},_ensureBgContainers:function _ensureBgContainers(){if(!this._bgContainer){this._bgContainer=document.createElement("div");this._bgContainer.id="background";this._bgRear=document.createElement("div");this._bgRear.id="backgroundRearLayer";this._bgContainer.appendChild(this._bgRear);this._bgFront=document.createElement("div");this._bgFront.id="backgroundFrontLayer";this._bgContainer.appendChild(this._bgFront);(0,_myAppnav.dom)(this.root).insertBefore(this._bgContainer,this.$.contentContainer)}},_getDOMRef:function _getDOMRef(id){switch(id){case"backgroundFrontLayer":this._ensureBgContainers();return this._bgFront;case"backgroundRearLayer":this._ensureBgContainers();return this._bgRear;case"background":this._ensureBgContainers();return this._bgContainer;case"mainTitle":return(0,_myAppnav.dom)(this).querySelector("[main-title]");case"condensedTitle":return(0,_myAppnav.dom)(this).querySelector("[condensed-title]");}return null},/**
   * Returns an object containing the progress value of the scroll effects
   * and the top position of the header.
   *
   * @method getScrollState
   * @return {Object}
   */getScrollState:function getScrollState(){return{progress:this._progress,top:this._top}}});// preventable, allowing for better scrolling performance.
(0,_myAppnav.setPassiveTouchGestures)(!0);// Set Polymer's root path to the same value we passed to our service worker
// in `index.html`.
(0,_myAppnav.setRootPath)(MyAppGlobals.rootPath);var MyApp=/*#__PURE__*/function(_PolymerElement){babelHelpers.inherits(MyApp,_PolymerElement);function MyApp(){babelHelpers.classCallCheck(this,MyApp);return babelHelpers.possibleConstructorReturn(this,babelHelpers.getPrototypeOf(MyApp).apply(this,arguments))}babelHelpers.createClass(MyApp,[{key:"_routePageChanged",value:function _routePageChanged(page){if("components"==page)page=void 0;if(!page){this.page="login"}else if(-1!==["view1","view2","view3"].indexOf(page)){this.page=page}else{this.page="view404"}if(!this.$.drawer.persistent){this.$.drawer.close()}}},{key:"_pageChanged",value:function _pageChanged(page){switch(page){case"login":new Promise(function(res,rej){return _require.default(["./my-login.js"],res,rej)}).then(function(bundle){return bundle&&bundle.$myLogin||{}});case"view1":new Promise(function(res,rej){return _require.default(["./my-view1.js"],res,rej)}).then(function(bundle){return bundle&&bundle.$myView1||{}});break;case"view2":new Promise(function(res,rej){return _require.default(["./my-view2.js"],res,rej)}).then(function(bundle){return bundle&&bundle.$myView2||{}});break;case"view3":new Promise(function(res,rej){return _require.default(["./my-view3.js"],res,rej)}).then(function(bundle){return bundle&&bundle.$myView3||{}});break;case"view404":new Promise(function(res,rej){return _require.default(["./my-view404.js"],res,rej)}).then(function(bundle){return bundle&&bundle.$myView404||{}});break;}}}],[{key:"template",get:function get(){return(0,_myAppnav.html$1)(_templateObject5_ba974b809c2211eaa17be74ebc068880())}},{key:"properties",get:function get(){return{page:{type:String,reflectToAttribute:!0,observer:"_pageChanged"},routeData:Object,subroute:Object}}},{key:"observers",get:function get(){return["_routePageChanged(routeData.page)"]}}]);return MyApp}(_myAppnav.PolymerElement);_exports.MyApp=MyApp;window.customElements.define("my-app",MyApp);var myApp={MyApp:MyApp};_exports.$myApp=myApp});