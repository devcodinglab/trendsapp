import{PolymerElement,html$1 as html}from"./my-appnav.js";import"./my-app.js";class DdlOptions extends PolymerElement{static get template(){return html` 
    <style include="shared-styles">
    :host {
      display: block;
    }

    select {
        width: 100%;
        height: 60px;
        font-size: 100%;
        cursor: pointer;
        border-radius: 0;
        background-color: #024481;
        border: none;
        color: white;
        appearance: none;
        padding: 15px 20px 15px 20px;
        -webkit-appearance: none;
        -moz-appearance: none;
        transition: color 0.3s ease, background-color 0.3s ease, border-bottom-color 0.3s ease;
      }

      option{
        min-height: 100px !important;
        padding: 15px 20px 15px 20px !important;
    }      

      /* For IE <= 11 */
      select::-ms-expand {
        display: none; 
      }
  
      .select-icon {
        position: absolute;
        top: 4px;
        right: 4px;
        width: 30px;
        height: 36px;
        pointer-events: none;
        border: 2px solid #962d22;
        padding-left: 5px;
        transition: background-color 0.3s ease, border-color 0.3s ease;
      }
      .select-icon svg.icon {
        transition: fill 0.3s ease;
        fill: white;
      }
  
      select:hover {
        color: #eeeeee;
        background-color: #024481;
      }

      select:hover ~ .select-icon {
        background-color: #eee;
      }

      select:hover ~ .select-icon svg.icon {
        fill: #fff;
      }

      </style>
    
    <select class="ddl-options" >
    <option value=''>{{label}}</option>
    <option value='1'>Opción 1</option>
    <option value='2'>Opción 2</option>
    <option value='3'>Opción 3</option>
    </select> 
    <div class="select-icon">
    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M0 7.33l2.829-2.83 9.175 9.339 9.167-9.339 2.829 2.83-11.996 12.17z"/></svg>
  </div>`}static get properties(){return{/**
       * `autoValidate` Set to true to auto-validate the input value.
       */label:{type:String,value:"Seleccione..."}}}}customElements.define("ddl-options",DdlOptions);class MyView2 extends PolymerElement{static get template(){return html`
      <style include="shared-styles">
        :host {
          display: block;

          padding: 10px;
        }
      </style>
      <my-app>
        <div class="card">
          <h1>Clasificación de Gastos</h1>
          <div class="txt-left-search">
            <txt-auto-complete label="Periodo"></txt-auto-complete>
          </div>
          <div class="txt-right-search">
            <ddl-options label="Categorías"></ddl-options>
          </div>
          <div class="coronita-list-view">
            <list-view
              id="lvw-classification"
              settings="[[_setSettings()]]"
              datasource="[[_setDataSource()]]"
            ></list-view>
          </div>
          <div class="list-action-set">
            <btn-action label="Ver más"></btn-action>
          </div>
        </div>
      </my-app>
    `}_setDataSource(){return[{//"_id" : ObjectId("5e8e40c84b3cf34f6206bec0"),
id:"2534546789",date:"04032020T13:23:32",recipient:{name:"WALMART"},amount:2300,contract:{number:"87654509",typeid:"CREDITCARD"},category:{id:"5e8d6ff8fbbe8af693ca6969",name:"despensa del hogar"},cardholder:{id:"5e8d6da8a387c752dc3e6886"}},{//"_id" : ObjectId("5e8e40c84b3cf34f6206bec1"),
id:"6787100098",date:"04032020T17:02:03",recipient:{name:"FARMACIAS DEL AHORRO"},amount:879.43,contract:{number:"87654509",typeid:"CREDITCARD"},category:{id:"5e8d6ff8fbbe8af693ca696a",name:"gastos m\xE9dicos"},cardholder:{id:"5e8d6da8a387c752dc3e6886"}},{//"_id" : ObjectId("5e8e40c84b3cf34f6206bec2"),
id:"6829998790",date:"05032020T16:10:07",recipient:{name:"RESTAURANTE HACIENDA"},amount:920.5,contract:{number:"87654509",typeid:"CREDITCARD"},category:{id:"5e8d6ff8fbbe8af693ca696b",name:"alimentos y bebidas"},cardholder:{id:"5e8d6da8a387c752dc3e6886"}},{//"_id" : ObjectId("5e8e40c84b3cf34f6206bec3"),
id:"5798320111",date:"05032020T10:42:53",recipient:{name:"VIPS"},amount:879.43,contract:{number:"87654509",typeid:"CREDITCARD"},category:{id:"5e8d6ff8fbbe8af693ca696b",name:"alimentos y bebidas"},cardholder:{id:"5e8d6da8a387c752dc3e6886"}},{//"_id" : ObjectId("5e8e40c84b3cf34f6206bec4"),
id:"7787122298",date:"05032020T11:04:23",recipient:{name:"PASION CAFE"},amount:59.43,contract:{number:"87654509",typeid:"CREDITCARD"},category:{id:"5e8d6ff8fbbe8af693ca696c",name:"gustos y habitos"},cardholder:{id:"5e8d6da8a387c752dc3e6886"}},{//"_id" : ObjectId("5e8e40c84b3cf34f6206bec5"),
id:"7775711199",date:"05032020T17:02:03",recipient:{name:"GASOLINERIA VERDE"},amount:1057.1,contract:{number:"87654509",typeid:"CREDITCARD"},category:{id:"5e8d6ff8fbbe8af693ca696d",name:"gastos fijos"},cardholder:{id:"5e8d6da8a387c752dc3e6886"}}]}_setSettings(){return[{headertext:"CONCEPTO",width:"10%",fieldname:"recipient.name"},{headertext:"FECHA",width:"10%",fieldname:"date"},{headertext:"MONTO",width:"10%",fieldname:"amount"},{headertext:"CATEGORIA",width:"10%",fieldname:"category.name"},{headertext:"",width:"10%",fieldname:"category.id",actionfield:"switch"}]}}window.customElements.define("my-view2",MyView2);