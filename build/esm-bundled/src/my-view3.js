import{PolymerElement,html$1 as html}from"./my-appnav.js";import"./my-app.js";class MyView3 extends PolymerElement{static get template(){return html`
      <style include="shared-styles">
        :host {
          display: block;

          padding: 10px;
        }
        
        .new-item {
          float: right;
          margin-right: 5%;
        }
      </style>

      <my-app>
      <div class="card">
        <h1>Admón de Categorías</h1>
        <div class="new-item">
          <action-item mode="link" label="Agregar Nuevo"></action-item>
        </div>
        <div class="coronita-list-view">
          <list-view  id="lvw-categories" settings="[[_setSettings()]]" datasource="[[_setDataSource()]]"></list-view>
        </div>
        <div class="list-action-set">
          <btn-action label="Ver más"></btn-action>
        </div>
      </div>
      </my-app>
    `}_setDataSource(){return[{//"_id" : ObjectId("5e8d6ff8fbbe8af693ca6969"),
id:"1",name:"despensa del hogar",cardholder:{id:"5e8d6da8a387c752dc3e6886"}},{//"_id" : ObjectId("5e8d6ff8fbbe8af693ca696a"),
id:"2",name:"gastos m\xE9dicos",cardholder:{id:"5e8d6da8a387c752dc3e6886"}},{//"_id" : ObjectId("5e8d6ff8fbbe8af693ca696b"),
id:"3",name:"alimentos y bebidas",cardholder:{id:"5e8d6da8a387c752dc3e6886"}},{//"_id" : ObjectId("5e8d6ff8fbbe8af693ca696c"),
id:"4",name:"gustos y habitos",cardholder:{id:"5e8d6da8a387c752dc3e6886"}},{//"_id" : ObjectId("5e8d6ff8fbbe8af693ca696d"),
id:"5",name:"gastos fijos",cardholder:{id:"5e8d6da8a387c752dc3e6886"}},{//"_id" : ObjectId("5e8d6ff8fbbe8af693ca696e"),
id:"6",name:"escuela de PA",cardholder:{id:"5e8d6da8a387c752dc3e6886"}}]}_setSettings(){return[{headertext:"CATEGORIA",width:"10%",fieldname:"name"},{headertext:"ACTIVO",width:"10%",fieldname:"id",actionfield:"switch"},{headertext:"EDITAR",width:"10%",fieldname:"id",actionfield:"edition"}]}}window.customElements.define("my-view3",MyView3);