define(["./my-appnav.js"],function(_myAppnav){"use strict";class MyView404 extends _myAppnav.PolymerElement{static get template(){return _myAppnav.html$1`
      <style>
        :host {
          display: block;

          padding: 10px 20px;
        }
      </style>

      Oops you hit a 404. <a href="[[rootPath]]">Head back to home.</a>
    `}}window.customElements.define("my-view404",MyView404)});