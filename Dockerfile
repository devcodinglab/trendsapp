#Imagen base
FROM node:latest
#Directorio de la aplicacion
WORKDIR /app
#Copiado de archivos
ADD build/es6-bundled /app/build/es6-bundled
ADD server.js /app
ADD package.json /app
#Dependencias
RUN npm install
#Puerto que expongo
EXPOSE 3000
#Comando
CMD ["npm", "start"]