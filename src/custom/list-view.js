import { PolymerElement, html } from "@polymer/polymer/polymer-element.js";
import "@polymer/polymer/lib/elements/dom-repeat.js";
import "./../shared-styles.js";
import "./action-item.js";

class ListView extends PolymerElement {
  static get template() {
    return html` <style include="shared-styles">
        :host {
          display: block;

          padding: 10px;
        }

        .list-view {
          width: 100%;
          margin: 0 0 0 0;
        }

        .list-view table {
          width: 100%;
          border-collapse: collapse;
          border-spacing: 0;
        }

        .list-view tr {
          height: 60px;
          background: #eef8ff;
          padding: 20px 30px 20px 30px;
        }

        .list-view tr:nth-child(odd) {
          background: #eef8ff;
        }

        .list-view tr:nth-child(odd) > td {
          border-top: 1px solid #c4c4c4;
          border-bottom: 1px solid #c4c4c4;
          border-left: 0px;
          boder-right: 0px;
        }

        .list-view tr:nth-child(even) {
          background: #fff;
        }

        .list-view tr > th {
          text-align: center;
          background: #fff;
          text-weight: bold;
        }

        .list-view tr > td {
          padding: 0 2% 0 2%;
          margin: 0 0 0 0;
        }
      </style>

      <div class="list-view">
        <table>
          <thead>
            <tr>
              <template is="dom-repeat" items="[[settings]]">
                <th>[[item.headertext]]</th>
              </template>
            </tr>
          </thead>
          <tbody>
            <template is="dom-repeat" items="[[datasource]]" as="datarow">
              <tr>
                <template is="dom-repeat" items="[[settings]]" as="datacolumn">
                  <template is="dom-if" if="[[_evalFieldType(datacolumn.actionfield)]]">
                    <td>[[_dataBind(datarow, datacolumn.fieldname)]]</td>
                  </template>
                  <template is="dom-if" if="[[!_evalFieldType(datacolumn.actionfield)]]">
                  <td><action-item mode="[[datacolumn.actionfield]]" isactive="true"></action-item></td>
                </template>
                </template>
              </tr>
            </template>
          </tbody>
        </table>
      </div>`;
  }

  static get properties() {
    return {
      /**
       * `autoValidate` Set to true to auto-validate the input value.
       */
      datasource: {
        type: Array,
        value: [
          {
            columnA: "AAAA",
            columnB: "AAAB",
            columnC: "AAAC",
            columnD: "AAAD",
            columnE: "AAAE",
          },
          {
            columnA: "BAAA",
            columnB: "BAAB",
            columnC: "BAAC",
            columnD: "BAAD",
            columnE: "BAAE",
          },
          {
            columnA: "CAAA",
            columnB: "CAAB",
            columnC: "CAAC",
            columnD: "CAAD",
            columnE: "CAAE",
          },
        ],
      },
      settings: {
        type: Array,
        value: [
          { headertext: "Column A", width: "10%", fieldname: "columnA" },
          { headertext: "Column B", width: "10%", fieldname: "columnB" },
          { headertext: "Column C", width: "10%", fieldname: "columnC" },
          { headertext: "Column D", width: "10%", fieldname: "columnD" },
          {
            headertext: "Column E",
            width: "10%",
            fieldname: "columnE",
            actionfield: "switch",
          },
        ],
      },
    };
  }

  _dataBind(row, column) {
    
    let result = row;
    
    let levels = column.split(".");

    for(let i=0; i<levels.length; i++)
    {
      let name = levels[i];

      result = result[name];
    }

    return result;
  }

  _evalFieldType(fieltype)
  {
    return (fieltype==undefined);
  }
}

window.customElements.define("list-view", ListView);
