import { PolymerElement, html } from "@polymer/polymer/polymer-element.js";
import "./../shared-styles.js";
import "./graph-label.js";

class GraphView extends PolymerElement {
  static get template() {
    return html`
    <style include="shared-styles">
        :host {
          display: block;
        }

        .graph-view-container {
          width: 60%;
          height: 400px;
          left: 20%;
          position: absolute;
        }

        .graph-view {
          width: 48%;
          height: 400px;
          float: left;
        }

        .graph-labels {
          width: 40%;
          height: 370px;
          float: right;
          padding: 30px 5% 0 5%;
        }

        ul{ 
            list-style: none; 
        } 
          
        ul li::before { 
              
            content: "\\25AA";   
            color: #009aed;  
            display: inline-block;  
            width: 0.8em; 
            margin-left: -0.9em; 
            font-weight: bold; 
            font-size:1.7rem; 
        } 

      </style>

      <div class="graph-view-container">
        <div class="graph-view">
        <template is="dom-repeat" items="{{graphs}}" as="graph">
        <div style="{{_evalGraph(graph)}}">
        </template>
        </div>
        <div class="graph-labels">
        <ul>
        <template is="dom-repeat" items="{{graphs}}" as="graph">
            <li>(7650.00) Despensa</li>
        </template
        </ul>
        </div>
      </div>
    `;
  }

  static get properties() {
    return {
      /**
       * `autoValidate` Set to true to auto-validate the input value.
       */
      graphs: {
        type: Array,
        value: [
          { val: "5000.00", color: "#0048ae", perc: "120" },
          { val: "5000.00", color: "#0065c2", perc: "105" },
          { val: "5000.00", color: "#009aed", perc: "75" },
          { val: "5000.00", color: "#0baae9", perc: "45" },
          { val: "5000.00", color: "#55b8dc", perc: "30" },
          { val: "5000.00", color: "#88c4d6", perc: "25" },
        ],
      },
    };
  }

  _evalGraph(graph) {
    return (
      "height: " +
      graph.perc +
      "px; width: 100%; background-color:" +
      graph.color +
      ";"
    );
    //#0048ae
  }
}
customElements.define("graph-view", GraphView);
