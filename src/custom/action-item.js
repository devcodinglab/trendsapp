import { PolymerElement, html } from "@polymer/polymer/polymer-element.js";
import "./../shared-styles.js";

class ActionItem extends PolymerElement {
  static get template() {
    return html`
    <style include="shared-styles">
    :host {
      display: block;
    }
        div.action-item {
          width: 100%;
          text-align: center;
        }
        div.action-item > button {
          cursor: pointer;
        }
        div.action-item-edition > button {
            background: none;
            border: none;
          }
          div.action-item-switch > button {
            background: none;
            border: none;
          }
          div.action-item-link> button {
            min-height:40px;
            min-width:150px;
            background: none;
            border: none;
            cursor: pointer;
            color: #1974b8;
            font-weight: bold;
            padding: 0 37px 0 37px;
          }
      </style>
      <dom-module id="action-type">
      <template is="dom-if" if="{{_evalType('button')}}">
        <div class="action-item"><button>{{label}}</button></div>
      </template>
      <dom-module id="action-type">
      <template is="dom-if" if="{{_evalType('link')}}">
        <div class="action-item action-item-link"><button>{{label}}</button></div>
      </template>
      <template is="dom-if" if="{{_evalType('switch')}}">
        <div class="action-item action-item-switch"><button>
        <template is="dom-if" if="{{isactive}}">
        <svg width="41" height="24" viewBox="0 0 41 24" fill="none" xmlns="http://www.w3.org/2000/svg">
<rect width="41" height="24" rx="12" fill="#3472B3"/>
<path fill-rule="evenodd" clip-rule="evenodd" d="M29 24C35.6274 24 41 18.6274 41 12C41 5.37258 35.6274 0 29 0C22.3726 0 17 5.37258 17 12C17 18.6274 22.3726 24 29 24Z" fill="white"/>
<path fill-rule="evenodd" clip-rule="evenodd" d="M28.7 23.25C35.0685 23.25 40.2312 18.2132 40.2312 12C40.2312 5.7868 35.0685 0.75 28.7 0.75C22.3315 0.75 17.1687 5.7868 17.1687 12C17.1687 18.2132 22.3315 23.25 28.7 23.25ZM41 12C41 18.6274 35.4931 24 28.7 24C21.9069 24 16.4 18.6274 16.4 12C16.4 5.37258 21.9069 0 28.7 0C35.4931 0 41 5.37258 41 12Z" fill="#3472B3"/>
</svg>
</template>
<template is="dom-if" if="{{!isactive}}">
<svg width="41" height="24" viewBox="0 0 41 24" fill="none" xmlns="http://www.w3.org/2000/svg">
<rect width="41" height="24" rx="12" fill="#666666"/>
<path fill-rule="evenodd" clip-rule="evenodd" d="M12 24C18.6274 24 24 18.6274 24 12C24 5.37258 18.6274 0 12 0C5.37258 0 0 5.37258 0 12C0 18.6274 5.37258 24 12 24Z" fill="white"/>
<path fill-rule="evenodd" clip-rule="evenodd" d="M12.3 23.25C18.6685 23.25 23.8313 18.2132 23.8313 12C23.8313 5.7868 18.6685 0.75 12.3 0.75C5.93147 0.75 0.76875 5.7868 0.76875 12C0.76875 18.2132 5.93147 23.25 12.3 23.25ZM24.6 12C24.6 18.6274 19.0931 24 12.3 24C5.5069 24 0 18.6274 0 12C0 5.37258 5.5069 0 12.3 0C19.0931 0 24.6 5.37258 24.6 12Z" fill="#666666"/>
</svg>
</template>
        </button></div>
      </template>
      <template is="dom-if" if="{{_evalType('edition')}}">
        <div class="action-item action-item-edition"><button>
        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path fill-rule="evenodd" clip-rule="evenodd" d="M1.84616 17.5376L12.8731 6.46048L14.7108 8.30691H17.468L14.2515 5.07586L17.1948 2.11916C17.5546 1.75777 18.1336 1.75353 18.4947 2.11629L21.7923 5.42885C22.1504 5.78866 22.1563 6.36605 21.7894 6.73463L6.44075 22.1531H1.84616V17.5376ZM22.1539 20.3077H11.0769L9.23078 22.1539H22.1539V20.3077Z" fill="#004481"/>
        <mask id="mask0" mask-type="alpha" maskUnits="userSpaceOnUse" x="1" y="1" width="22" height="22">
        <path fill-rule="evenodd" clip-rule="evenodd" d="M1.84616 17.5376L12.8731 6.46048L14.7108 8.30691H17.468L14.2515 5.07586L17.1948 2.11916C17.5546 1.75777 18.1336 1.75353 18.4947 2.11629L21.7923 5.42885C22.1504 5.78866 22.1563 6.36605 21.7894 6.73463L6.44075 22.1531H1.84616V17.5376ZM22.1539 20.3077H11.0769L9.23078 22.1539H22.1539V20.3077Z" fill="white"/>
        </mask>
        <g mask="url(#mask0)">
        <rect width="24" height="24" fill="#1973B8"/>
        </g>
        </svg>
        </button></div>
      </template>
    </dom-module>
    `;
  }

  static get properties() {
    return {
      label: {
        type: String,
        value: "Click",
      },
      mode: {
        type: String,
        value: "button",
      },
      isactive: {
        type: Boolean,
        value: false,
      },     
    };
  }

  _evalType(ctrlType) {
    return this.mode == ctrlType;
  };
}
customElements.define("action-item", ActionItem);
