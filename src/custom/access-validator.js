import { PolymerElement, html } from "@polymer/polymer/polymer-element.js";
import "./../shared-styles.js";
import "./action-item.js";

import "@polymer/app-route/app-location.js";
import "@polymer/app-route/app-route.js";
import "../data/access-data-validation.js";

class AccessValidator extends PolymerElement {
  static get template() {
    return html`
      <style include="shared-styles">
        :host {
          display: block;
        }

        .access-validator {
          width: 25%;
        }

        .main-title {
          color: #004481;
          font-size: 30px;
          font-weight: bold;
        }

        .pass-recover {
          margin-top: 20px;
        }

        div.hidden {
          display: none;
        }

        p.incorrect-data {
          color: red;
        }
      </style>
      <div class="access-validator">
        <div class="main-title">TRENDS</div>
        <paper-input on-focus="_hiddeIncorrect"
          id="User_name"
          type="number"
          name="usser"
          float-label
          label="Número"
        ></paper-input>
        <paper-input on-focus="_hiddeIncorrect"
          id="Password"
          type="password"
          name="Password"
          float-label
          label="Contraseña"
        ></paper-input>
        <div class="card-actions">
          <paper-button
            raised
            on-tap="_login"
            class="indigo"
            style="background:#237aba;color:#fff;width:100%"
            >Entrar</paper-button
          >
          <div class="pass-recover">
            <action-item
              mode="link"
              label="¿Olvidaste tu contraseña?"
            ></action-item>
          </div>
          <div style="display:[[_displaymode]];">
            <p class="incorrect-data">Datos incorrectos</p>
          </div>
        </div>
      </div>
      <app-location route="{{route}}"></app-location>
      <app-route
        route="{{route}}"
        pattern="/:view"
        data="{{routeData}}"
        tail="{{subroute}}"
      >
      </app-route>
      <access-data-validation
        id="usrDo"
        url="[[url]]"
        result="{{_isvalid}}"
        params="[[_data]]"
      ></access-data-validation>
    `;
  }

  static get properties() {
    return {
      /**
       * `autoValidate` Set to true to auto-validate the input value.
       */
      url: {
        type: String,
        value: "http://localhost:3001/v0/login",
      },
      _data: Object,
      _login: {
        type: Function,
      },
      _isvalid: {
        type: Boolean,
        observer: "_evalResult",
      },
      _displaymode: {
        type: String,
        value: "none",
      },
      route: Object,
      routeData: Object,
      subroute: Object,
    };
  }

  _login() {
    this._data = {
      number: this.$.User_name.value,
      password: this.$.Password.value,
    };

    this.$.usrDo.execValidation();
  }

  _evalResult(out, old) {
    if (old != undefined) {
      let isValid = out.response;

      if (isValid) this.set("route.path", "view1");

      else this._displaymode = "block";
    }
  }

  _hiddeIncorrect(){
    this._displaymode = "none";
  }

}
customElements.define("access-validator", AccessValidator);
