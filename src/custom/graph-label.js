import { PolymerElement, html } from "@polymer/polymer/polymer-element.js";
import "./../shared-styles.js";

class GraphLabel extends PolymerElement {
  static get template() {
    return html` 
    <style include="shared-styles">
    :host {
      display: block;
    }
    .graph-label{
        height:35px;
        width:90%;
    }

    .bullet-point{
        width:15px;
        height:15px;
        background-color: #009aed;
    }

    </style>
    <div class="graph-label">
    <div class="bullet-point"></div><label>(7650.00) Despensa</label>
    </div>
    `;
  }

  static get properties() {
    return {
      /**
       * `autoValidate` Set to true to auto-validate the input value.
       */
      label: {
        type: String,
        value: 'Click',
      },
    };
  }
}
customElements.define("graph-label", GraphLabel);