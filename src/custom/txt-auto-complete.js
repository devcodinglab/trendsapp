import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@fluidnext-polymer/paper-autocomplete/paper-autocomplete.js';

class TxtAutoComplete extends PolymerElement {
  static get template() {
    return html`
      <paper-autocomplete label="{{label}}" source={{array}}></paper-autocomplete>
    `;
  }

  static get properties() {
    return {
      /**
       * `autoValidate` Set to true to auto-validate the input value.
       */
      label: {
        type: String,
        value: 'Buscar',
      },
    };
  }
}
customElements.define('txt-auto-complete', TxtAutoComplete);