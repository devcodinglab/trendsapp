import { PolymerElement, html } from "@polymer/polymer/polymer-element.js";
import "./../shared-styles.js";

class DdlOptions extends PolymerElement {
  static get template() {
    return html` <style include="shared-styles">
        :host {
          display: block;
        }

        select {
          width: 100%;
          height: 60px;
          font-size: 100%;
          cursor: pointer;
          border-radius: 0;
          background-color: #024481;
          border: none;
          color: white;
          appearance: none;
          padding: 15px 20px 15px 20px;
          -webkit-appearance: none;
          -moz-appearance: none;
          transition: color 0.3s ease, background-color 0.3s ease,
            border-bottom-color 0.3s ease;
        }

        option {
          min-height: 100px !important;
          padding: 15px 20px 15px 20px !important;
        }

        /* For IE <= 11 */
        select::-ms-expand {
          display: none;
        }

        .select-icon {
          position: absolute;
          top: 4px;
          right: 4px;
          width: 30px;
          height: 36px;
          pointer-events: none;
          border: 2px solid #962d22;
          padding-left: 5px;
          transition: background-color 0.3s ease, border-color 0.3s ease;
        }
        .select-icon svg.icon {
          transition: fill 0.3s ease;
          fill: white;
        }

        select:hover {
          color: #eeeeee;
          background-color: #024481;
        }

        select:hover ~ .select-icon {
          background-color: #eee;
        }

        select:hover ~ .select-icon svg.icon {
          fill: #fff;
        }
      </style>

      <select class="ddl-options">
        <option value="">[[label]]</option>
        <template is="dom-repeat" items="{{dataSource}}" as="row">
          <option value=[[row.id]]">[[row.name]]</option>
        </template>
      </select>
      <div class="select-icon">
      </div>`;
  }

  static get properties() {
    return {
      /**
       * `autoValidate` Set to true to auto-validate the input value.
       */
      label: {
        type: String,
        value: "Seleccione...",
      },
      dataSource: {
        type: Object,
        value: [
          {
            id: "1",
            name: "despensa del hogar",
          },
          {
            id: "2",
            name: "gastos médicos",
          },
          {
            id: "3",
            name: "alimentos y bebidas",
          },
          {
            id: "4",
            name: "gustos y habitos",
          },
          {
            id: "5",
            name: "gastos fijos",
          },
          {
            id: "6",
            name: "escuela de PA",
          },
        ],
        notify: true,
      },
    };
  }
}
customElements.define("ddl-options", DdlOptions);
