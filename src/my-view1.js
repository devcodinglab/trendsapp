import { PolymerElement, html } from "@polymer/polymer/polymer-element.js";
import "./shared-styles.js";
import "./custom/txt-auto-complete.js";
import "./custom/graph-view.js";
import { MyApp } from "./my-app.js"

class MyView1 extends PolymerElement {
  static get template() {
    return html`
      <style include="shared-styles">
        :host {
          display: block;

          padding: 10px;
        }

        .coronita-graph-view {
          width: 100%;
          height: 500px;
          padding: 100px 0 0 0;
        }
      </style>
      <my-app>
      <div class="card">
        <h1>Tendencia Financiera</h1>
        <div class='txt-left-search'>
        <txt-auto-complete label='Periodo'></txt-auto-complete>
        </div>
        <div class="coronita-graph-view">
          <graph-view></graph-view>
        </div>
      </div>
      </my-app>
    `;
  }
}

window.customElements.define("my-view1", MyView1);
