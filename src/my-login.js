import { PolymerElement, html } from "@polymer/polymer/polymer-element.js";
import "@polymer/paper-card/paper-card.js";
import "@polymer/paper-button/paper-button.js";
import "@polymer/paper-input/paper-input.js";
import "./custom/access-validator.js";

class Mylogin extends PolymerElement {
  constructor() {
    super();
    console.log("none");
  }
  ready() {
    super.ready();
  }
  static get template() {
    return html`
      <style include="shared-styles">
        :host {
          display: block;

          padding: 10px;
        }
        .center {
          width: 100%;
          text-align: center;
          margin-top: 200px;
          position: absolute;
          left: 37%
        }
      </style>
      <div class="center">
        <access-validator></access-validator>
      </div>
    `;
  }
}
window.customElements.define("my-login", Mylogin);
