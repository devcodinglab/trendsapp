import { PolymerElement, html } from "@polymer/polymer/polymer-element.js";
import { MyApp } from "./my-app.js";
import "./shared-styles.js";

import "./custom/list-view.js";
import "./custom/txt-auto-complete.js";
import "./custom/btn-action.js";
import "./custom/ddl-options.js";

import "./data/categories-data-binder.js";

class MyView2 extends PolymerElement {
  static get template() {
    return html`
      <style include="shared-styles">
        :host {
          display: block;

          padding: 10px;
        }
      </style>
      <my-app>
        <div class="card">
          <h1>Clasificación de Gastos</h1>
          <div class="txt-left-search">
            <txt-auto-complete label="Periodo"></txt-auto-complete>
          </div>
          <div class="txt-right-search">
            <ddl-options
              id="ddlCategories"
              label="Categorías"
              fieldValue=""
              fieldText=""
            ></ddl-options>
          </div>
          <div class="coronita-list-view">
            <list-view
              id="lvw-classification"
              settings="[[_setSettings()]]"
              datasource="[[_setDataSource()]]"
            ></list-view>
          </div>
          <div class="list-action-set">
            <btn-action label="Ver más"></btn-action>
          </div>
        </div>
        <categories-data-binder
          httpmethod="GET"
          resultType="ctg"
          result="{{_categoriesList}}"
        ></categories-data-binder>
      </my-app>
    `;
  }

  static get properties() {
    return {
      _categoriesList: {
        type: Object,
        value: [],
        observer: "_evalResult",
      },
    };
  }

  _setDataSource() {
    return [
      {
        //"_id" : ObjectId("5e8e40c84b3cf34f6206bec0"),
        id: "2534546789",
        date: "04032020T13:23:32",
        recipient: {
          name: "WALMART",
        },
        amount: 2300.0,
        contract: {
          number: "87654509",
          typeid: "CREDITCARD",
        },
        category: {
          id: "5e8d6ff8fbbe8af693ca6969",
          name: "despensa del hogar",
        },
        cardholder: {
          id: "5e8d6da8a387c752dc3e6886",
        },
      },
      {
        //"_id" : ObjectId("5e8e40c84b3cf34f6206bec1"),
        id: "6787100098",
        date: "04032020T17:02:03",
        recipient: {
          name: "FARMACIAS DEL AHORRO",
        },
        amount: 879.43,
        contract: {
          number: "87654509",
          typeid: "CREDITCARD",
        },
        category: {
          id: "5e8d6ff8fbbe8af693ca696a",
          name: "gastos médicos",
        },
        cardholder: {
          id: "5e8d6da8a387c752dc3e6886",
        },
      },
      {
        //"_id" : ObjectId("5e8e40c84b3cf34f6206bec2"),
        id: "6829998790",
        date: "05032020T16:10:07",
        recipient: {
          name: "RESTAURANTE HACIENDA",
        },
        amount: 920.5,
        contract: {
          number: "87654509",
          typeid: "CREDITCARD",
        },
        category: {
          id: "5e8d6ff8fbbe8af693ca696b",
          name: "alimentos y bebidas",
        },
        cardholder: {
          id: "5e8d6da8a387c752dc3e6886",
        },
      },
      {
        //"_id" : ObjectId("5e8e40c84b3cf34f6206bec3"),
        id: "5798320111",
        date: "05032020T10:42:53",
        recipient: {
          name: "VIPS",
        },
        amount: 879.43,
        contract: {
          number: "87654509",
          typeid: "CREDITCARD",
        },
        category: {
          id: "5e8d6ff8fbbe8af693ca696b",
          name: "alimentos y bebidas",
        },
        cardholder: {
          id: "5e8d6da8a387c752dc3e6886",
        },
      },
      {
        //"_id" : ObjectId("5e8e40c84b3cf34f6206bec4"),
        id: "7787122298",
        date: "05032020T11:04:23",
        recipient: {
          name: "PASION CAFE",
        },
        amount: 59.43,
        contract: {
          number: "87654509",
          typeid: "CREDITCARD",
        },
        category: {
          id: "5e8d6ff8fbbe8af693ca696c",
          name: "gustos y habitos",
        },
        cardholder: {
          id: "5e8d6da8a387c752dc3e6886",
        },
      },
      {
        //"_id" : ObjectId("5e8e40c84b3cf34f6206bec5"),
        id: "7775711199",
        date: "05032020T17:02:03",
        recipient: {
          name: "GASOLINERIA VERDE",
        },
        amount: 1057.1,
        contract: {
          number: "87654509",
          typeid: "CREDITCARD",
        },
        category: {
          id: "5e8d6ff8fbbe8af693ca696d",
          name: "gastos fijos",
        },
        cardholder: {
          id: "5e8d6da8a387c752dc3e6886",
        },
      },
    ];
  }

  _setSettings() {
    return [
      { headertext: "CONCEPTO", width: "10%", fieldname: "recipient.name" },
      { headertext: "FECHA", width: "10%", fieldname: "date" },
      { headertext: "MONTO", width: "10%", fieldname: "amount" },
      { headertext: "CATEGORIA", width: "10%", fieldname: "category.name" },
      {
        headertext: "",
        width: "10%",
        fieldname: "category.id",
        actionfield: "switch",
      },
    ];
  }

  _evalResult(out, old) {
    if (out != undefined) this.$.ddlCategories.DataSource = out;
  }
}

window.customElements.define("my-view2", MyView2);
