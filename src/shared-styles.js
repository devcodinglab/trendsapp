import '@polymer/polymer/polymer-element.js';

const $_documentContainer = document.createElement('template');
$_documentContainer.innerHTML = `<dom-module id="shared-styles">
  <template>
    <style>
      .card {
        margin: 24px;
        padding: 16px;
        color: #757575;
        border-radius: 5px;
        background-color: #fff;
        box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 1px 5px 0 rgba(0, 0, 0, 0.12), 0 3px 1px -2px rgba(0, 0, 0, 0.2);
      }

      .card > h1{
        color: #212121;
        font-size: 22px;
        text-align: center;
        margin: 0px auto;
        padding: 20px 0px 15px 0px;  
        width: 100%;
        display:inline-block;
      }

      .circle {
        display: inline-block;
        width: 64px;
        height: 64px;
        text-align: center;
        color: #555;
        border-radius: 50%;
        background: #ddd;
        font-size: 30px;
        line-height: 64px;
      }

      .coronita-list-view {
        width: 100%;
        margin: 70px 0 0 0;
      }

      div.txt-left-search{
        width:30%;
        float: left;
      }

      div.txt-right-search{
        width:30%;
        float: right;
      }

      div.txt-right-search > ddl-options{
        width: 100%;
        height: 50px;
      }

      div.list-action-set{
        margin-top: 30px;
        width:100%;
        text-align: center;
      }


    </style>
  </template>
</dom-module>`;

document.head.appendChild($_documentContainer.content);
