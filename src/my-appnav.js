import { PolymerElement, html } from "@polymer/polymer/polymer-element.js";
import {
  setPassiveTouchGestures,
  setRootPath,
} from "@polymer/polymer/lib/utils/settings.js";
import "@polymer/app-layout/app-scroll-effects/app-scroll-effects.js";
import "@polymer/app-layout/app-toolbar/app-toolbar.js";
import "@polymer/app-route/app-location.js";
import "@polymer/app-route/app-route.js";
import "@polymer/iron-pages/iron-pages.js";
import "@polymer/iron-selector/iron-selector.js";
import "@polymer/paper-icon-button/paper-icon-button.js";
import "./my-icons.js";

setPassiveTouchGestures(true);

setRootPath(MyAppGlobals.rootPath);

class MyAppNav extends PolymerElement {
  static get template() {
    return html`
      <app-location route="{{route}}" url-space-regex="^[[rootPath]]">
      </app-location>
      <app-route
        route="{{route}}"
        pattern="[[rootPath]]:page"
        data="{{routeData}}"
        tail="{{subroute}}"
      >
      </app-route>
      <iron-pages selected="[[page]]" attr-for-selected="name" role="main">
        <my-login name="login"></my-login>
        <my-view1 name="view1"></my-view1>
        <my-view2 name="view2"></my-view2>
        <my-view3 name="view3"></my-view3>
      </iron-pages>
    `;
  }
  static get properties() {
    return {
      page: {
        type: String,
        reflectToAttribute: true,
        observer: "_pageChanged",
      },
      routeData: Object,
      subroute: Object,
    };
  }
  static get observers() {
    return ["_routePageChanged(routeData.page)"];
  }
  _routePageChanged(page) {

    if (!page) {
      this.page = "login";
    } else if (
      ["login", "myapp", "view1", "view2", "view3"].indexOf(page) !== -1
    ) {
      this.page = page;
    } else {
      this.page = "view404";
    }
  }
  _pageChanged(page) {
    switch (page) {
      case "myapp":
        import("./my-app.js");
        break;
      case "login":
        import("./my-login.js");
        break;
      case "view1":
        import("./my-view1.js");
        break;
      case "view2":
        import("./my-view2.js");
        break;
      case "view3":
        import("./my-view3.js");
        break;
      case "view404":
        import("./my-view404.js");
        break;
    }
  }
}
window.customElements.define("my-appnav", MyAppNav);
