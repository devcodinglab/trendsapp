import { PolymerElement, html } from "@polymer/polymer/polymer-element.js";
import "./../shared-styles.js";
import "@polymer/iron-ajax/iron-ajax.js";

class AccessDataValidation extends PolymerElement {
  static get template() {
    return html`
      <iron-ajax
        id="AjaxPost"
        url="{{url}}"
        method="POST"
        content-type="application/json"
        handle-as="json"
        on-response="_onresult"
        on-error="_onerror"
      >
      </iron-ajax>
    `;
  }

  static get properties() {
    return {
      url: {
        type: String,
        value: "",
      },
      params: Object,
      result: {
        type: Boolean,
        value: false,
        notify: true,
      }
    };
  }

  execValidation() {
    this.$.AjaxPost.body = this.params;
    this.$.AjaxPost.generateRequest();
  }

  _onresult(data) {
    this.result = data.detail.__data.xhr;
  }

  _onerror(e, req) {
    this.result = { response: false };
  }
}
customElements.define("access-data-validation", AccessDataValidation);
