import { PolymerElement, html } from "@polymer/polymer/polymer-element.js";
import "../shared-styles.js";
import "@polymer/iron-ajax/iron-ajax.js";

class CategoriesData extends PolymerElement {
  static get template() {
    return html`
      <iron-ajax
        id="AjaxData"
        url="{{url}}"
        method="[[httpmethod]]"
        content-type="application/json"
        handle-as="json"
        on-response="_onresult"
        on-error="_onerror"
      >
      </iron-ajax>
    `;
  }

  static get properties() {
    return {
      url: {
        type: String,
        value: "",
      },
      httpmethod: {
          type:String,
          value:"GET"
      },
      params: Object,
      result: {
        type: Object,
        value: {},
        notify: true,
      }
    };
  }

  execValidation() {
    if(this.httpmethod != "GET")
        this.$.AjaxData.body = this.params;
    
    this.$.AjaxData.generateRequest();
  }

  _onresult(data) {
    this.result = data.detail.__data.xhr;
  }

  _onerror(e, req) {
    this.result = { response: false };
  }
}
customElements.define("categories-data", CategoriesData);