import { PolymerElement, html } from "@polymer/polymer/polymer-element.js";
import "../shared-styles.js";
import "@polymer/iron-ajax/iron-ajax.js";
import "./categories-data.js";

class CategoriesDataBinder extends PolymerElement {
  static get template() {
    return html`
      <categories-data
        id="categoriesData1"
        httpmethod="[[httpmethod]]"
        url="[[_evalUrl(_url, cardholderid)]]"
        result="{{result}}"
        params="[[params]]"
      ></categories-data>
    `;
  }

  static get properties() {
    return {
      _url: {
        type: String,
        value: "http://localhost:3001/v0/cardholders/{cardholderid}/categories",
      },
      httpmethod: {
        type: String,
        value: "",
      },
      fieldsSet: {
        type: Object,
        value: ["id", "name"],
      },
      params: Object,
      resultType: {
        type: String,
        value: "ctg",
      },
      result: {
        type: Object,
        observer: "_evalResult",
      },
    };
  }

  connectedCallback(){
      this.$.categoriesData1.execValidation();
  }

  _evalUrl(urlBase) {
    const cardholderId = "5e8d6da8a387c752dc3e6886";

    let url = urlBase.replace("{cardholderid}", cardholderId);

    return url;
  }

  _evalAsCatalog(data) {

    let list = [];

    if (data != undefined) {
      let field1 = this.fieldsSet[0];
      let field2 = this.fieldsSet[1];

      list = data.map((item) => {
        return { id: item[field1], name: item[field2] };
      });
    }

    return list;
  }

  _evalAsTable(data) {
    return {};
  }

  _evalResult(out, old) {
    if (out != undefined) {
      switch (this.resultType) {
        case "tbl":
          return this._evalAsTable(out.response);
        case "ctg":
          return this._evalAsCatalog(out.response);
      }
    }
  }
}
customElements.define("categories-data-binder", CategoriesDataBinder);
